# Framework do testowania automatycznego UI.



## 1. Cel projektu:

Napisanie freamworka do testów automatycznych UI i wykożystanie go do zautomatyzowania testów aplikacji: [Automation Exercise](https://automationexercise.com/)

---

## 2. Założenia projektu:

- [x] wykorzystanie Page Object Pattern
- [x] uruchamianie testów na różnych przeglądarkach
- [x] logowanie przebiegu testów
- [x] konfiguracja frameworka w pliku *properties*
- [x] konfiguracja testów w pliku xml (TestNG)
- [x] tworzenie raportów z wykonania testów (Allure)
- [x] uruchamianie testów wielowątkowo
- [x] parametryzacja przeglądarki per klasa testowa
- [x] tworzenie zrzutów ekranu w przypadku porażki testu
- [x] powtarzanie testów w przypadku porażki
- [x] uruchamianie testów w ramach ciągłej integracji
- [x] uruchamianie testów w ramach Selenium GRID
- [ ] sprzątanie po testach
- [x] możliwość wczytania danych testowtch z pliku excel
- [x] udostępnianie logów przeglądarki

---

## 3. Pokryte scenariusze testowe:

   <details><summary>Test Case 1: Register user</summary>

        1. Launch browser
        2. Navigate to url 'http://automationexercise.com/login'
        3. Enter name and email address
        4. Click 'Signup' button
        5. Fill details: Title, Name, Email, Password, Date of birth
        6. Fill details: First name, Last name, Company, Address, Address2, Country, State, City, Zipcode, Mobile Number
        7. Click 'Create Account button'
        8. Verify that 'ACCOUNT CREATED!' is visible

   </details>

<details><summary>Test Case 2: Register user with exist email</summary>

        1. Launch browser
        2. Navigate to url 'http://automationexercise.com/login'
        3. Enter name and exist email address
        4. Click 'Signup' button
        5. Verify that 'Email Address alredy exist!' is visible

   </details>

   <details><summary>Test Case 3: Login user with correct email and password</summary>

        1. Launch browser
        2. Navigate to url 'http://automationexercise.com/login'
        3. Enter correct email address and password
        4. Click 'login' button
        5. Verify that 'Logged in as username' is visible

   </details>
   <details><summary>Test Case 4: Login user with incorrect email and password</summary>

        1. Launch browser
        2. Navigate to url 'http://automationexercise.com/login'
        3. Enter incorrect email address and password
        4. Click 'login' button
        5. Verify error 'Your email or password is incorrect!' is visible

   </details>

   <details><summary>Test Case 5: Logout user</summary>

        1. Launch browser
        2. Navigate to url 'http://automationexercise.com/login'
        3. Enter correct email address and password
        4. Click 'login' button
        5. Verify that 'Logged in as username' is visible
        6. Click 'Logout' button
        7. Verify that user is navigated to login page

   </details>
   
   <details><summary>Test Case 6: Contact us form</summary>

        1. Launch browser
        2. Navigate to url 'http://automationexercise.com/contact_us'
        3. Enter name, email, subject and message
        4. Upload file
        5. Click 'Submit' button
        6. Click OK button
        7. Verify success message 'Success! Your details have been submitted successfully.' is visible

   </details>
    <details><summary>Test Case 7: Use contact us form without email address</summary>

        1. Launch browser
        2. Navigate to url 'http://automationexercise.com/contact_us'
        3. Enter name, subject and message
        4. Upload file
        5. Click 'Submit' button
        6. Verify success message  isn't displayed

   </details>
   <details><summary>Test Case 8: Verify all products and product detail page</summary>

        1. Launch browser
        2. Navigate to url 'http://automationexercise.com/products'
        3. Click on 'View Product' of first product
        4. Verify that detail detail is visible: product name, category, price, availability, condition, brand

   </details>
   <details><summary>Test Case 9: Search product</summary>

        1. Launch browser
        2. Navigate to url 'http://automationexercise.com/products'
        3. Enter product name in search input
        4. Click search button
        5. Verify all the products related to search are visible

   </details>
   <details><summary>Test Case 10: Verify subscription in home page</summary>

        1. Launch browser
        2. Navigate to url 'http://automationexercise.com'
        3. Enter email address in input
        4. Click arrow button
        5. Verify success message 'You have been successfully subscribed!' is visible

   </details>
   <details><summary>Test Case 11: Verify subscription in cart page</summary>

        1. Launch browser
        2. Navigate to url 'http://automationexercise.com/view_cart'
        3. Enter email address in input 
        4. Click arrow button
        5. Verify success message 'You have been successfully subscribed!' is visible

   </details>
   <details><summary>Test Case 12: Add products in cart</summary>

        1. Launch browser
        2. Navigate to url 'http://automationexercise.com/products'
        3. Add products to cart
        8. Verify all products are added to Cart

   </details>
    <details><summary>Test Case 13: Add products in cart from excel file</summary>

        1. Launch browser
        2. Navigate to url 'http://automationexercise.com/products'
        3. Add products from file to cart
        7. Verify all products are added to Cart

   </details>
    <details><summary>Test Case 14: Place order: Login before checkout</summary>

        1. Launch browser
        2. Navigate to url 'http://automationexercise.com/login'
        3. Fill email, password
        4. Click 'Login' button
        5. Verify 'Logged in as username' at top
        6. Add 3 products to cart
        7. Verify all products are added to Cart
        8. Click Proceed To Checkout
        9. Click 'Place Order'
        10. Enter payment details: Name on Card, Card Number, CVC, Expiration date
        11. Click 'Pay and Confirm Order' button
        12. Verify success message 'Your order has been placed successfully!'

</details>
    <details><summary>Test Case 15: Place order: Login while checkout</summary>

        1. Launch browser
        2. Navigate to url 'http://automationexercise.com/products'
        3. Add 3 products to cart
        4. Verify all products are added to Cart
        5. Click Proceed To Checkout
        6. Click Register/Login link
        7. Fill email and password
        8. Click 'Login' button
        9. Verify 'Logged in as username' at top
        10. Click Cart link
        11. Click Proceed To Checkout
        12. Click Place Order
        13. Enter payment details: Name on Card, Card Number, CVC, Expiration date
        14. Click 'Pay and Confirm Order' button
        15. Verify success message 'Your order has been placed successfully!'

 </details>
<details><summary>Test Case 16: Remove product from cart</summary>

        1. Launch browser
        2. Navigate to url 'http://automationexercise.com/products'
        3. Add different products to cart
        4. Verify all products are added to Cart
        5. Remove product from Cart (Click 'X' button corresponding to particular product)
        6. Verify that product is removed from the Cart

 </details>
<details><summary>Test Case 17: Remove product from cart</summary>

        1. Launch browser
        2. Navigate to url 'http://automationexercise.com/products'
        3. Add  the same products to cart
        4. Verify all products are added to Cart
        5. Remove product from Cart (Click 'X' button corresponding to particular product)
        6. Verify that the Cart is empty

 </details>
<details><summary>Test Case 18: Remove product from cart</summary>

        1. Launch browser
        2. Navigate to url 'http://automationexercise.com/products'
        3. Add different products to cart
        4. Verify all products are added to Cart
        5. Remove all product from Cart (Click 'X' button corresponding to particular product)
        6. Verify that the Cart is empty

 </details>
<details><summary>Test Case 19: Add review on product</summary>

       1. Launch browser
       2. Navigate to url 'http://automationexercise.com'
       3. Click on 'View Product' button
       4. Enter name, email and review
       5. Click 'Submit' button
       6. Verify success message 'Thank you for your review.'

 </details>
<details><summary>Test Case 20: Add review on product without email</summary>

       1. Launch browser
       2. Navigate to url 'http://automationexercise.com'
       3. Click on 'View Product' button
       4. Enter name and review
       5. Click 'Submit' button
       6. Verify success message 'Thank you for your review' is not displayed

 </details>
<details><summary>Test Case 21: Verify Scroll Up using 'Arrow' button</summary>

       1. Launch browser
       2. Navigate to url 'http://automationexercise.com'
       3. Scroll down page to bottom
       4. Verify the page is scrolled down
       5. Click on arrow at bottom right side to move upward
       6. Verify that page is scrolled up 

 </details>
<details><summary>Test Case 22: Verify Scroll Up without 'Arrow' button</summary>

       1. Launch browser
       2. Navigate to url 'http://automationexercise.com'
       3. Scroll down page to bottom
       4. Verify the page is scrolled down
       5. Scroll up page
       6. Verify that page is scrolled up

 </details>

---

## 4. Wykorzystane freamworki i oprogramowanie:

- __Java__ - obiektowy język programowania
- __Selenium WebDriver__ - narzędzie pozwalające na automatyzację testów aplikacjach internetowych
- __TestNG__ - biblioteka programistyczna służąca do pisania testów
- __Apache Maven__ - narzędzie automatyzujące budowę oprogramowania
- __AssertJ__ - biblioteka dostarczająca bogaty zestaw asercji
- __JavaFaker__ - biblioteka dostarczająca losowych danych
- __Apache log4j__ - biblioteka służąca do zapisywania logów zdarzeń
- __Apache poi__ - biblioteka ułatwiająca przetważane plików Excel
- __Allure__ - freamework pozwalający tworzyć raporty z testów automatycznych
- __Owner__ - biblioteka pozwalająca w łatwy sposób odczytywać pliki *.properties*
- __Git__ - rozproszony system kontroli wersji
- __Jenkins__ - serwer ciągłej integracji
- __Docker__ - oprogramowanie służące do konteryzacji aplikacji
- __Selenium GRID__ - oprogramowanie umożliwiające uruchamianie testów równolegle na różnych systemach operacyjnych i różnych przeglądarkach
- __Markdown__ - język znaczników ułatwiający pisanie dokumentacji

---

## 5. Opis pakietów i klas:

- __pages__ - zawiera klasy page objectów reprezentujących poszczególne strony aplikacji
- __tests__ - zawiera klasy testowe
- __utils__ - pakiet narzędziowy zawiera klasy pomocnicze: do obsługi plików excel, logowania zdarzeń przeglądarki, obsługi okien alertów, dostarczające dane do testów
- __driver__ - zawiera klasę BrowserFactory dostarczająca drivera wybranej przeglądarki
- __driver.manager__ -  zawiera klasę DriverManager zarządzającą driverem z wykożystaniem klasy opakowującej ThredLocal, która zapewnia każdemu wątkowi lokalną kopię drivera. Klasa DriverOptions ustawia opcje dla odpowiedniego typu przeglądarki.
- __driver.listeners__ - zawiera klasę DriverEventListener w której implementujemy wybrane metody interfejsu WebDriverListener, metody te będą logowały zdarzenia akcji Selenium. Klasa WebDriverEventListenerRegister dekoruje drivera w listenery.
- __assertions__ - zawiera klasę udostępniającą metody asercji webelementów zaimplementowane zgodnie z instrukcjami ze strony [AssertJ Assertion Generator](http://joel-costigliola.github.io/assertj/assertj-assertions-generator.html)
- __waits__ - zawiera klasę udostępniającą metody czekające na webelementy
- 
---

## 6. Opis implementacji

### 6.1 Struktura freamworka

W projekcje używany jest wzorzec projektowy __Page Object__, którego twórcą jest __Martin Fowler__, jest to wzorzec projektowy, który zakłada przedstawienie stron internetowych ich części lub funkcjonalności jako objektów programistycznych. 
Obiekty te posiadają webelementy które odpowiadają elementom strony (przyciski, pola tekstowe, pola wyboru, linki itp) oraz metody które odzwierciedlają działania jakie
możemy na tych elementach wykonać (wpisz tekst, kliknij przycisk itp). Wprowadzenie tego wzorca ma na celu odseparowanie stron lub elementów stron od testów, 
testy stają się czytelniejsze, prostsze w implementacji i utrzymaniu, zmniejsz sie też duplikacja kodu. 

Poniżej kod przykładowej klasy, jest to klasa odzwierciedlająca stronę płatności:

```java
...

public class PaymentPage extends BasePage {

    @FindBy(css = "input[name='name_on_card']") private WebElement cardNameInput;
    @FindBy(css = "input[name='card_number']") private WebElement cardNumberInput;
    @FindBy(css = "input[name='cvc']") private WebElement cvcInput;
    @FindBy(css = "input[name='expiry_month']") private WebElement expiryMonthInput;
    @FindBy(css = "input[name='expiry_year']") private WebElement expiryYearInput;
    @FindBy(id = "submit") private WebElement submitButton;

    public PaymentPage() { super(); }

    @Step("Type into Card Name field: {cardName}")
    public PaymentPage typeCardName(String cardName) {
        WaitForWebElement.waitUntilElementIsVisible(cardNameInput);
        type(cardNameInput, cardName);
        log().info("Type card name: {}", cardName);
        return this;
    }

    @Step("Type into Card Number field: {cardNumber}")
    public PaymentPage typeCardNumber(String cardNumber) {
        type(cardNumberInput, cardNumber);
        log().info("Type card number: {}", cardNumber);
        return this;
    }

    @Step("Type into CVC field: {cvc}")
    public PaymentPage typeCVC(String cvc) {
        type(cvcInput, cvc);
        log().info("Type cvc: {}", cvc);
        return this;
    }

    @Step("Type into Expiration Month field: { expirationMonth}")
    public PaymentPage typeExpirationMonth(String expirationMonth) {
        type(expiryMonthInput, expirationMonth);
        log().info("Type expiration month: {}", expirationMonth);
        return this;
    }

    @Step("Type into Expiration year: {expirationYear}")
    public PaymentPage typeExpirationYear(String expirationYear) {
        type(expiryYearInput, expirationYear);
        log().info("Type expiration year: {}", expirationYear);
        return this;
    }

    @Step("Click on Submit button")
    public PaymentDonePage clickSubmitButton() {
        click(submitButton);
        return new PaymentDonePage();
    }
    ...
}
```
Klasa zawiera prywatne pola odpowiadające elementą strony i metody działające na tych elementach. Metody  zwracają 
__Page Objecty__ co pozwala na zastosowanie w klasach testowych koncepcji __Fluent Interfaces__ czyli ciągłego budowania 
kodu tworzącego logiczny ciąg. 

Przykładowa metoda testowa:

```java
public void addReviewOnProduct() {
        DriverUtils.navigateTo(ApplicationURLs.APPLICATION_URL);

        HomePage homePage = new HomePage();

        homePage
                .clickOnViewProductLink()
                .typeName(DataGiver.getName())
                .typeEmail(DataGiver.getEmail())
                .typeReview(DataGiver.getLongText())
                .clickOnSubmitButton()
                .assertThatInformationIsDisplayed("Thank you for your review.");
    }
```

Do inicjalizacji __WebElementów__ wykożystywana jest klasa __PageFactory__ i metodę _initElements()_. Żeby nie 
wywoływać tej metody w każdej klasie __Page Object__ to wywołujemy ją w konstruktorze klasy abstrakcyjnej 
__BasePage__ po której dziedziczą wszystkie __Page Object__:

```java
...
public abstract class BasePage {
    private Logger logger = LogManager.getLogger(this.getClass().getName());

    public BasePage() {  PageFactory.initElements(DriverManager.getDriver(), this);  }

    protected Logger log() { return logger; }

    public void type(final WebElement webElement, String string) {
        webElement.clear();
        webElement.sendKeys(string);
    }

    public void sendFile(final WebElement webElement, String filePath) {
        if(configuration.isRemoteRun()) ((RemoteWebElement)webElement).setFileDetector(new LocalFileDetector());
        type(webElement, filePath);
    }

    public void click(final WebElement webElement) {  webElement.click();  }
    public WebElement find(SearchContext searchContext, By locator) {  return searchContext.findElement(locator);
    public WebElement find(By locator) { return DriverManager.getDriver().findElement(locator);  }
    public List<WebElement> finds(By locator) { return DriverManager.getDriver().findElements(locator); }
    public void select(WebElement webElement, String string) { new Select(webElement).selectByValue(string); }
}
  ```
Do metody _initElements()_ przekazujemy obiekt klasy __WebDriver__ oraz obiekt klasy reprezentującej stronę. W klasie __BasePage__ mamy
jeszcze zaimplementowane logowanie zdarzeń. Dodatkowo w klasie __BasePage__ mamy zdefiniowane metody opakowujące
metody __Selenium__, z których korzystają klasy __Page Objectów__. Powstaje w ten sposób pewna abstrakcja oddzielająca klasy 
__Page Objectów__ od __Selenium__. Co nam to daje? Freamwork __Selenium__ jest cały czas rozwijany jedne metody stają 
się przestażałe i zostaja zastępowane przez inne. Na przykład metody _getLocation()_ i _getSize()_ w __Selenium 4__ zostały 
zastąpione przez metodę _getRect()_. W przypadku gdy jakaś metoda __Selenium__ wyjdzie z użycia i zostanie zastąpiona inną
nie musimy jej szukać po wszystkich __Page Objectach__, zamieniamy ją tylko w jednym miejscu w klasie __BasePage__. Dzięki takiej 
konstrukcji freamwork jest podzielony na pewne warstwy: __testy, page objecty, abstrakcja BasePage, Selenium i strona web__. 
Warsywy te są od siebie oddzielone i komunikują się tylko z warstwami sąsiednimi. Testy rozmawiają tylko z __Page 
Objectami__, __Page Objecty__ rozmawiają z __BasePage__, ale już nie z __Selenium__.

```
                  Tests
----------------------------------------
               Page objects
----------------------------------------
                 BasePage
----------------------------------------
                 Selenium
----------------------------------------
              Web Application                                                       
```

Podsumowując strukturę projektu mamy:
- strony są reprezentowane przez klasy
- elementy strony są reprezentowane przez pola klas
- akcje są reprezentowane przez metody klas
- metody Page Objectów zwracają nowe Page Objecty
- testy komunikują się jedynie z Page Objectami
- Page Objecty komunikują się z BasePage
- BasePage komunikuje się z driverem

### 6.2 Zarządzanie WebDriverem

Zarzadzanie WebDriverem opiera się o klasę __DriverManager__ . 
Ponieważ framework umożliwia wykonywanie testów wielowątkowo a także parametryzację przeglądarki dla testów dodatkowo
testy mogą być uruchamiane na __Selenium GRID__ lub lokalnie, wszystko  to sprawia że klasy te są troche rozbudowane.
Klasa __DriverManager__ jest oparta na wzorcu projektowym __Singleton__, czyli dba o to żeby było tylko jedno wystąpienie obiektu WebDriver dla 
testu. Odpowiada za to metoda _getDriver()_, która sprawdza czy instancja WebDrivera jest zainicjalizowana jeśli tak to ją zwraca w 
przeciwnym wypadku rzuca wyjątek.

```java
    public static WebDriver getDriver() {
        if (webDriverThreadLocal.get() == null) {
            throw new IllegalStateException("WebDriver Instance was null! Please create instance of WebDriver using setDriver!");
        }
        return webDriverThreadLocal.get();
    }
```
WebDriver jako obiekt statyczny jest przechowywany w pamięci tupu sterta i z tego powodu jest współdzielony przez
wszystkie wątki. Żeby testy mogły wykonywać się wielowątkowo framework musi zadbać aby dla każdego
wątku dostępna była inna instancja WebDrivera. Zapewnia to wykożystanie  klasy __ThreadLocal__, która dla każdego wątku zainicjuje
niezależną kopię zmiennej. 

Klasa __ThreadLocal__ posiada cztery metody:
- _set()_ - ustawia zmienną dla bierzącego wątku.
- _get()_ - zwraca zmienną dla bierzącego wątku
- _remove()_ - usuwa zmienną dla bierzącego wątku
- _initialValue()_ - inicjuje zmienną wartością domyślną

Zmienne które muszą mieć niezależne instancje dla każdego wątku to WebDriver i typ wybranej przeglądarki reprezentowany 
przez enum BrowserType.

```java
 private static ThreadLocal<WebDriver> webDriverThreadLocal = new ThreadLocal<>();
 private static ThreadLocal<BrowserType> browserTypeThreadLocal = new ThreadLocal<>();
```

W metodzie _setDriver()_  ustawiamy dla wątku zmienne _browserType_ i _webDriver_ za pomocą metody _set()_
z klasy __ThreadLocal__ . Dodatkowo zmienna _webDriver_ inicjowana jest odpowiednim typem drivera dostarczanym
przez klasę __BrowserFactory__.

```java
public static void setDriver(BrowserType browserType) {
        WebDriver webDriver = null;

        if (browserType == null) {
            browserType = BROWSER_TYPE;
            webDriver = new BrowserFactory(browserType, IS_REMOTE_RUN).getBrowser();
        } else {
            webDriver = new BrowserFactory(browserType, IS_REMOTE_RUN).getBrowser();
        }
        webDriver = WebDriverEventListenerRegister.registerWebDriverEventListener(webDriver);
        browserTypeThreadLocal.set(browserType);
        webDriverThreadLocal.set(webDriver);
    }
```

Metoda _disposeDriver()_ zamyka przeglądarkę internetową. Sprawdzenie czy typ przeglądarki to FireFox podyktowane
jest faktem że Gekodriver inaczej obsługuję metodę _close()_ niż inne drivery.

```java
 public static void disposeDriver() {
        webDriverThreadLocal.get().close();
        if (!browserTypeThreadLocal.get().equals(FIREFOX)) {
            webDriverThreadLocal.get().quit();
        }
        webDriverThreadLocal.remove();
        browserTypeThreadLocal.remove();
    }
```

### 6.3 Parametryzacja przeglądarki

Za dostarczanie odpowiedniego typu przeglądarki odpowiedzialna jest klasa __BrowserFactory__ i metoda _getBrowser()_.
Metoda _getBrowser()_ zwraca obiekt typu __WebDriver__ w zależności od tego czy testy odbywają się lokalnie czy zdalnie
dostarcza odpowiednio: konkretnej implementacji drivera(EdgeDriver, FireFoxDriver, itp) lub dla testów zdalnych 
na Selenium GRID - __RemoteWebDrivera__. Odpowiedni typ przeglądarki/drivera jest pobierany z dwóch różnych miejsc.
Jeśli testy wykonywane są lokalnie i nie korzystany z plików _.xml_ do parametryzacji testów to rodzaj przeglądarki pobierany jest
z pliku _.properties_ (jest to wartość klucza __DEFAULT_BROWSER__). Jeżeli testy wykonywane są 
zdalnie z wykożystaniem plików _.xml_ do konfiguracji zestawów testowych to wlaśnie z tych plików pobierana jest 
wersja przeglądarki dla poszczególnych testów.

### 6.4 Konfiguracja frameworka (plik .properties)

Konfiguracja freamworka odbywa się w pliku konfiguracyjnym _EnvironmentConfig.properties_ w pliku tym
wartości konfiguracyjne zapisane są w postacji __klucz = wartość__: 

```properties
APP_URL = https://automationexercise.com
DEFAULT_BROWSER = EDGE

IS_REMOTE_RUN = false
GRID_URL= http://localhost:4444/wd/hub
```
poszczególne wartości odpowiadają za:

- __APP_URL__ - adres url testowane aplikacji
- __DEFAULT_BROWSER__ - ustawienie domyślnej przeglądarki dla testów (wpisywane dużymi literami ze względu na to
że wartości w enumie __BrowserType__ też są pisane dużymi literami)
- __IS_REMOTE_RUN__ - wartość oznacza czy wykonujemy testy na Selenium GRID(wartość: __true__), czy lokalnie
(wartość: __false__)
- __GRID_URL__ - adres url Selenium GRID

### 6.5 Konfiguracja zestawu testów (plik .xml) 

Zestawy testów są konfigurowane za pomocą mechanizmu __TestNG__ w plikach _.xml_. Pliki te określają które
testy mają się wykonać, na jakich przeglądarkach i na ilu wątkach.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE suite SYSTEM "https://testng.org/testng-1.0.dtd" >
<suite name="Automation Exercise" parallel="methods" thread-count="3">

    <listeners>
        <listener class-name="utils.testng.listeners.TestListeners"/>
        <listener class-name="utils.testng.listeners.AnnotationTransformer"/>
    </listeners>

    <test name="Login Tests">
       <parameter name="browser" value="EDGE"/>
        <classes>
            <class name="tests.LoginUserTest"/>
        </classes>
    </test>
    
   ...
    
    <test name="Add Review On Product Tests">
        <parameter name="browser" value="FIREFOX"/>
        <classes>
            <class name="tests.AddReviewOnProductTest"/>
        </classes>
    </test>

    <test name="Remove From Cart">
        <parameter name="browser" value="CHROME"/>
        <classes>
            <class name="tests.RemoveFromCartTest"/>
        </classes>
    </test>
</suite>
```
Tag `<test>` określa testy które będą wykonywane, można je podać jako całe klasy testowe, jak w pliku powyżej
lub jako poszczególne metody lub pakiety. 
Ilość wątków określana jest w tagu `<suit>` za pomocą atrybutu _thread-count_ 
(`<suite name="Automation Exercise" parallel="methods" thread-count="3">`),  natomiast rodzaj przeglądarki określany jest
dla poszczególnych testów za pomocą tagu `<parameter>`, w którym podana jest nazwa i wartość parametru
(`<parameter name="browser" value="CHROME"/>`).

### 6.10 Uruchamianie testów w ramach ciągłej integracji

Do uruchamiania testów w środowisku ciągłej integracii wykorzystujemy serwer __Jenkins__ i __Selenium GRID__.
Serwer Jenkins uruchamia prosty pipeline:
```json5
pipeline {
    tools {
              maven 'Maven'
        }
    agent any
    stages {
        stage('Build test code') {
            steps {
                bat 'mvn clean install -DskipTests'
            }
        }
        stage('Run selenium grid') {
            steps {
                bat 'docker-compose up -d'
            }
        }
        stage('Execute test') {
            steps {
                bat 'mvn test'
            }
        }
    }
    post {
        always {
            bat 'docker-compose down'
            script {
                allure([
                        includeProperties: false,
                        jdk              : '',
                        properties       : [],
                        reportBuildPolicy: 'ALWAYS',
                        results          : [[path: 'target/allure-results']]
                ])
            }
        }
    }
}
```
pipeline składa się z kilku prostych kroków:
- zbudowanie testów
- uruchomienie Selenium GRID na Dockerze
- uruchonienie testów
- zatrzymanie Dockera
- wygenerowanie raportów Allure

W myśl zasady __infrastructure as a code__ Selenium GRID jest uruchamiane przez serwer Jenkins
z wykożystaniem pliku _dockre-compose.yml_. 

```dockerfile
version: "3"
services:
  chrome:
    image: selenium/node-chrome:4.11.0-20230801
    shm_size: 2gb
    depends_on:
      - selenium-hub
    environment:
      - SE_EVENT_BUS_HOST=selenium-hub
      - SE_EVENT_BUS_PUBLISH_PORT=4442
      - SE_EVENT_BUS_SUBSCRIBE_PORT=4443
      - SE_NODE_OVERRIDE_MAX_SESSIONS=true
      - SE_NODE_MAX_SESSIONS=3

  edge:
    image: selenium/node-edge:4.11.0-20230801
    shm_size: 2gb
    depends_on:
      - selenium-hub
    environment:
      - SE_EVENT_BUS_HOST=selenium-hub
      - SE_EVENT_BUS_PUBLISH_PORT=4442
      - SE_EVENT_BUS_SUBSCRIBE_PORT=4443
      - SE_NODE_OVERRIDE_MAX_SESSIONS=true
      - SE_NODE_MAX_SESSIONS=3

  firefox:
    image: selenium/node-firefox:4.11.0-20230801
    shm_size: 2gb
    depends_on:
      - selenium-hub
    environment:
      - SE_EVENT_BUS_HOST=selenium-hub
      - SE_EVENT_BUS_PUBLISH_PORT=4442
      - SE_EVENT_BUS_SUBSCRIBE_PORT=4443
      - SE_NODE_OVERRIDE_MAX_SESSIONS=true
      - SE_NODE_MAX_SESSIONS=3

  selenium-hub:
    image: selenium/hub:4.11.0-20230801
    container_name: selenium-hub
    ports:
      - "4442:4442"
      - "4443:4443"
      - "4444:4444"
```

Na Dockerze uchamiany jest Hub z trzema nodami, każdy z nodów może obsłużyć maksymalnie do trzech sesji i
dostarcza system Linux z odpowiednią przeglądarką.

---

## 7. Co dalej?

- [ ] rozszerzenie klasy LoadableComponent przez klasy page objectów, aby zapewnić jednakowy sposób sprawdzania czy strona została załadowana prawidłowo
- [ ] wyodrębnienie z page objectów do odrębnych klas funkcjonalności dostępnych na kilku stronach np: wyszukiwanie, subskrypcja.
- [ ] dodanie mapy lokalizatorów w postaci pliku _properties_ jako centralnej bazy lokalizatorów
- [ ] tworzenie i usuwanie kont użytkowników dla testów przez API za pomocą REST Assured
- [x] zmiana projektu tak aby klasy page objectów nie wywoływały bezpośrednio metod Selenium (sendKeys(), click(), etc.) tylko pośrednio po przez klase BasePage
    

