package configuration;

import driver.BrowserType;
import org.aeonbits.owner.Config;

@Config.Sources("classpath:EnvironmentConfig.properties")
public interface EnvironmentConfig  extends Config {

    @Key("APP_URL")
    String appUrl();

    @Key("DEFAULT_BROWSER")
    BrowserType defaultBrowser();

    @Key("IS_REMOTE_RUN")
    boolean isRemoteRun();

    @Key("GRID_URL")
    String gridUrl();
}
