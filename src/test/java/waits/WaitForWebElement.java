package waits;

import driver.manager.DriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class WaitForWebElement {
    private static Logger logger = LogManager.getLogger(WaitForWebElement.class);

    private static WebDriverWait getWebDriverWait() {
        return new WebDriverWait(DriverManager.getDriver(), Duration.ofSeconds(10));
    }

    public static void waitUntilElementIsVisible(WebElement element) {
        logger.info("WAIT======>  Waiting until element is visible: '{}'", element);
        WebDriverWait webDriverWait = getWebDriverWait();
        webDriverWait.until(ExpectedConditions.visibilityOf(element));
        logger.info("WAIT======>  Element is visible: '{}'", element);
    }

    public static void waitUntilElementIsVisible(By locator) {
        logger.info("WAIT======>  Waiting until element is visible: '{}'", locator);
        WebDriverWait webDriverWait = getWebDriverWait();
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        logger.info("WAIT======>  Element is visible: '{}'", locator);
    }

    public static void waitUntilElementIsClickable(WebElement element) {
        logger.info("WAIT======> Waiting until element is clickable: '{}'", element);
        WebDriverWait webDriverWait = getWebDriverWait();
        webDriverWait.until(ExpectedConditions.elementToBeClickable(element));
        logger.info("WAIT======> Element is clickable: '{}'", element);
    }

    public static void waitUntilElementIsClickable(By locator) {
        logger.info("WAIT======> Waiting until element is clickable: '{}'", locator);
        WebDriverWait webDriverWait = getWebDriverWait();
        webDriverWait.until(ExpectedConditions.elementToBeClickable(locator));
        logger.info("WAIT======> Element is clickable: '{}'", locator);
    }

    public static void wait(By locator) {
        logger.info("WAIT======> Waiting until element is clickable: '{}'", locator);
        WebDriverWait webDriverWait = getWebDriverWait();
        try{
            Thread.sleep(3000);
        }catch (Exception e){}
        logger.info("WAIT======> Element is clickable: '{}'", locator);
    }

}
