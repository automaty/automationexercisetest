package driver.manager;

import driver.BrowserFactory;
import driver.BrowserType;
import driver.listeners.WebDriverEventListenerRegister;
import org.openqa.selenium.WebDriver;

import static configuration.Configuration.*;
import static driver.BrowserType.*;


public class DriverManager {
    private static ThreadLocal<WebDriver> webDriverThreadLocal = new ThreadLocal<>();
    private static ThreadLocal<BrowserType> browserTypeThreadLocal = new ThreadLocal<>();
    private static final BrowserType BROWSER_TYPE = configuration.defaultBrowser();
    private static final boolean IS_REMOTE_RUN = configuration.isRemoteRun();

    private DriverManager() {}

    public static void setDriver(BrowserType browserType) {
        WebDriver webDriver = null;

        if (browserType == null) {
            browserType = BROWSER_TYPE;
            webDriver = new BrowserFactory(browserType, IS_REMOTE_RUN).getBrowser();
        } else {
            webDriver = new BrowserFactory(browserType, IS_REMOTE_RUN).getBrowser();
        }
        webDriver = WebDriverEventListenerRegister.registerWebDriverEventListener(webDriver);
        browserTypeThreadLocal.set(browserType);
        webDriverThreadLocal.set(webDriver);
    }

    public static WebDriver getDriver() {
        if (webDriverThreadLocal.get() == null) {
            throw new IllegalStateException("WebDriver Instance was null! Please create instance of WebDriver using setDriver!");
        }
        return webDriverThreadLocal.get();
    }

    public static void disposeDriver() {
        webDriverThreadLocal.get().close();
        if (!browserTypeThreadLocal.get().equals(FIREFOX)) {
            webDriverThreadLocal.get().quit();
        }
        webDriverThreadLocal.remove();
        browserTypeThreadLocal.remove();
    }
}
