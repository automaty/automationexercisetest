package driver.manager;


import io.qameta.allure.Step;

public class DriverUtils {
    @Step("Maximizing browser window")
    public static void setInitialConfiguration() {
        DriverManager.getDriver().manage().window().maximize();
    }

    @Step("Navigate to URL: {url}")
    public static void navigateTo(String url) {
        DriverManager.getDriver().navigate().to(url);
    }

    @Step("Get page title")
    public static String getPageTitle() {
        return DriverManager.getDriver().getTitle();
    }

}
