package driver.manager;

import driver.BrowserType;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;

import java.util.logging.Level;

public class DriverOptions {

    public static Object getOptions(BrowserType browserType) {
        LoggingPreferences loggingPreferences = new LoggingPreferences();
        loggingPreferences.enable(LogType.BROWSER, Level.INFO);

        switch (browserType) {
            case CHROME:
                ChromeOptions chromeOptions = new ChromeOptions();
              //  chromeOptions.setCapability(CapabilityType.LOGGING_PREFS, loggingPreferences);
                chromeOptions.addArguments("--remote-allow-origins=*");
                chromeOptions.merge(chromeOptions);
                return chromeOptions;
            case EDGE:
                EdgeOptions edgeOptions = new EdgeOptions();
//                edgeOptions.setCapability(CapabilityType.LOGGING_PREFS, loggingPreferences);
                   edgeOptions.addArguments("--remote-allow-origins=*");
                   edgeOptions.merge(edgeOptions);
                return edgeOptions;
            case FIREFOX:
                FirefoxOptions firefoxOptions = new FirefoxOptions();
                // firefoxOptions.setCapability(CapabilityType.LOGGING_PREFS, loggingPreferences);
                firefoxOptions.merge(firefoxOptions);
                return firefoxOptions;
            default:
                throw new IllegalStateException("Unsupported browser!!!!!");
        }
    }
}
