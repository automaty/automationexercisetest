package driver;

import configuration.Configuration;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import static configuration.Configuration.*;
import static driver.manager.DriverOptions.getOptions;

public class BrowserFactory {

    private static final String MESSAGE_UNKNOWN_BROWSER = "Unknown browser type! Please check your configuration.";

    private BrowserType browserType;
    private boolean isRemoteRun;

    public BrowserFactory(BrowserType browserType, boolean isRemoteRun) {
        this.browserType = browserType;
        this.isRemoteRun = isRemoteRun;
    }

    public  WebDriver getBrowser()  {
        if (isRemoteRun) {
            DesiredCapabilities desiredCapabilities = new DesiredCapabilities();

            switch (browserType) {
                case CHROME:
                    desiredCapabilities.merge(new ChromeOptions());
                    return  getRemoteDriver(desiredCapabilities);
                case EDGE:
                    desiredCapabilities.merge(new EdgeOptions());
                    return getRemoteDriver(desiredCapabilities);
                case FIREFOX:
                    desiredCapabilities.merge(new FirefoxOptions());
                    return getRemoteDriver(desiredCapabilities);
                default:
                    throw new IllegalStateException(MESSAGE_UNKNOWN_BROWSER);
            }
        } else {
            switch (browserType) {
                case CHROME:
                    WebDriverManager.chromedriver().setup();
                    return new ChromeDriver((ChromeOptions) getOptions(browserType));
                case EDGE:
                    WebDriverManager.edgedriver().setup();
                    return new EdgeDriver((EdgeOptions) getOptions(browserType));
                case FIREFOX:
                    WebDriverManager.firefoxdriver().setup();
                    return new FirefoxDriver((FirefoxOptions) getOptions(browserType));
                default:
                    throw new IllegalStateException(MESSAGE_UNKNOWN_BROWSER);
            }
        }
    }

    private RemoteWebDriver getRemoteDriver(DesiredCapabilities capabilities) {
        try {
            return new RemoteWebDriver(new URL(configuration.gridUrl()), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to create RemoteWebDriver."  + e.getMessage());
        }
    }
}
