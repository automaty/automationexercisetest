package utils.testng.listeners;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import utils.ScreenShotMaker;

public class TestListeners implements ITestListener {

    private Logger logger = LogManager.getLogger(TestListeners.class);

    @Override
    public void onTestStart(ITestResult result) {
       logger.info("Starting test: " + result.getName());
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        logger.info("Test " + result.getName() +" passed successfully");
    }

    @Override
    public void onTestFailure(ITestResult result) {
        logger.info("Test " + result.getName() + " failed!");
        ScreenShotMaker.makeScreenShot();
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        logger.info("Test " + result.getName() + " skipped!");
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
       logger.info("Test " + result.getName() + " failed!");
       ScreenShotMaker.makeScreenShot();
    }

    @Override
    public void onTestFailedWithTimeout(ITestResult result) {
        logger.info("Test " + result.getName() + " failed!");
        ScreenShotMaker.makeScreenShot();
    }

    @Override
    public void onStart(ITestContext context) {}

    @Override
    public void onFinish(ITestContext context) {}
}
