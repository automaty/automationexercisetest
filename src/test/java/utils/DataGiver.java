package utils;

import com.github.javafaker.Faker;
import com.github.javafaker.service.FakeValuesService;
import com.github.javafaker.service.RandomService;

import java.util.Locale;

public class DataGiver {
    public static final String CORRECT_EMAIL = "darth.vader@deathstar.imp";
    public static final String CORRECT_PASSWORD = "DarthVader";
    public static final String USER_NAME = "Darth Vader";
    public static final String INCORRECT_LOGIN = "Your email or password is incorrect!";
    public static final String CONTACT_US_SUCCESS_MESSAGE = "Success! Your details have been submitted successfully.";
    public static final String SUBSCRIPTION_SUCCESS_MESSAGE = "You have been successfully subscribed!";
    public static final String FILE_PATH = "C:\\Users\\sebastian.cichonski\\IdeaProjects\\AutomationexerciseTest\\src\\test\\resources\\test.txt";


    private static Faker faker = new Faker();
    private static FakeValuesService fakeValuesService = new FakeValuesService(new Locale("pl-PL"), new RandomService());

    public static String getEmail() {
        return faker.internet().emailAddress();
    }

    public static String getPassword() {
        return faker.internet().password();
    }

    public static String getName(){
        return faker.name().username();
    }

    public static String getFirstName(){
        return faker.name().firstName();
    }

    public static String getLastName(){
        return faker.name().lastName();
    }

    public static String getCompany(){
        return faker.company().name();
    }

    public static String getCity(){
        return  faker.address().cityName();
    }
    public static String getStreet(){
        return faker.address().streetAddress();
    }

    public static String getState(){
        return faker.address().state();
    }

    public static String getZipcode(){
        return faker.address().zipCode();
    }

    public static String getMobileNumber(){
        return faker.phoneNumber().cellPhone();
    }

    public static String getRandomData() {
        return fakeValuesService.bothify("??????#????????#????????????##??????###??");
    }

    public static String getCardName() {
        return faker.business().creditCardType();
    }

    public static String getCardNumber() {
        return faker.business().creditCardNumber();
    }

    public static String getCVC() {
        return fakeValuesService.bothify("???");
    }

    public static String getExpirationCardMonth() {
        return faker.regexify("((^0[1-9]$)|(^1[0-2]$))");
    }

    public static String getExpirationCardYear() {//TODO dodaj regex
        return "2031";
    }

    public static String getShortText() {
        return faker.lorem().sentence();
    }

    public static String getLongText() {
        return faker.lorem().sentence(10,999);
    }
}
