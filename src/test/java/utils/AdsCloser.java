package utils;

import driver.manager.DriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class AdsCloser {
    private WebDriver driver;

    public AdsCloser() {
        this.driver = DriverManager.getDriver();
    }

    public void close() {

        int counter = 0;
        int size;
        do {    //TODO dodać logi
            List<WebElement> outerFrames = driver.findElements(By.tagName("iframe"));
            size = outerFrames.size();
            WebElement frame = outerFrames.get(counter);
            driver.switchTo().frame(frame);

            List<WebElement> innerFrames = driver.findElements(By.tagName("iframe"));
            for (WebElement innerFrame : innerFrames) {
                if (innerFrame.getAttribute("id").equals("ad_iframe")) {
                    driver.switchTo().frame(innerFrame);
                    driver.findElement(By.tagName("span")).click();
                    break;
                }
            }
            counter++;
            driver.switchTo().defaultContent();
        } while (counter < size);
    }
}
