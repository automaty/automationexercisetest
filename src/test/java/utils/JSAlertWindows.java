package utils;

import driver.manager.DriverManager;
import org.openqa.selenium.Alert;

public class JSAlertWindows {
    private Alert alert;

    public JSAlertWindows() {
        alert = DriverManager.getDriver().switchTo().alert();
    }

    public void confirm() {
        alert.accept();
    }

    public void cancel() {
        alert.dismiss();
    }
}
