package utils.excel;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Excel {
    private Sheet sheet;

    public Excel(String fileName, String sheetName) throws IOException {
        File file = new File("src/test/resources/" + fileName);
        FileInputStream fileInputStream = new FileInputStream(file);
        sheet = getWorkbook(fileName, fileInputStream).getSheet(sheetName);
    }

    public List<String> getColumn(int numberOfColumn) {
        List<String> cellList = new ArrayList<>();

        for (int i = 1; i <= getRowCount(sheet); i++) {
            cellList.add(sheet.getRow(i).getCell(numberOfColumn).getStringCellValue());
        }
        return cellList;
    }

    private Object[][] getCellValues(Sheet sheet) {
        Object[][] data = new Object[getRowCount(sheet)][getColumnCount(sheet)];

        for (int i = 0; i <= getRowCount(sheet); i++) {
            for (int j = 0; j < getColumnCount(sheet); j++) {
                data[i - 1][j] = sheet.getRow(i).getCell(j).getStringCellValue();
            }
        }
        return data;
    }

    private String getFileExt(String fileName) {
        return fileName.substring(fileName.indexOf("."));
    }

    private Workbook getWorkbook(String fileName, FileInputStream fileInputStream) throws IOException {
        if (getFileExt(fileName).equals(".xlsx")) {
            return new XSSFWorkbook(fileInputStream);
        } else if (getFileExt(fileName).equals(".xls")) {
            return new HSSFWorkbook(fileInputStream);
        } else throw new IllegalStateException("Unknown file extension, please check the file");
    }

    private int getRowCount(Sheet sheet) {
        return sheet.getLastRowNum();
    }

    private int getColumnCount(Sheet sheet) {
        return sheet.getRow(0).getLastCellNum();
    }
}
