package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;

import java.util.Date;

public class BrowserLogging {

    public static void getBrowserLog(WebDriver driver) {
            LogEntries browserLogs = driver.manage().logs().get(LogType.BROWSER);

            System.out.println("================== Logi przeglądarki (JavaScript Console) =======================");
            for (LogEntry entry : browserLogs) {

                System.out.println(new Date(entry.getTimestamp()) + " " + entry.getLevel() + " " + entry.getMessage());

                //assertFalse(entry.getLevel().equals(Level.SEVERE));  TODO sprawdzić
            }
            System.out.println("=====================================================================");

        }
}
