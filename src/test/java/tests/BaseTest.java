package tests;

import driver.BrowserType;
import driver.manager.DriverManager;
import driver.manager.DriverUtils;
import io.qameta.allure.Step;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import utils.BrowserLogging;


public abstract class BaseTest {

    @Step("Setting up browser to: {browserType}")
    @Parameters("browser")
    @BeforeMethod
    public void beforeMethod(@Optional BrowserType browserType) {
        DriverManager.setDriver(browserType);
        DriverManager.getDriver();
        DriverUtils.setInitialConfiguration();
    }

    @Step("Disposing browser")
    @AfterMethod
    public void afterMethod() {
        BrowserLogging.getBrowserLog(DriverManager.getDriver());
        DriverManager.disposeDriver();
    }

}
