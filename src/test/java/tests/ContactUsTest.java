package tests;

import driver.manager.DriverUtils;
import io.qameta.allure.*;
import org.testng.annotations.Test;
import pages.ContactUsPage;
import utils.DataGiver;

import static navigation.ApplicationURLs.CONTACTUS_URL;
import static utils.DataGiver.CONTACT_US_SUCCESS_MESSAGE;
import static utils.DataGiver.FILE_PATH;

public class ContactUsTest extends BaseTest {

    @Test
    @Issue("DEFECT-11")
    @TmsLink("TC-6")
    @Severity(SeverityLevel.NORMAL)
    @Description("The goal of this test is try to use contact form")
    public void asUserTryToUsingContactUsForm() {
        DriverUtils.navigateTo(CONTACTUS_URL);

        ContactUsPage contactUsPage = new ContactUsPage();

        contactUsPage
                .typeName(DataGiver.getName())
                .typeEmail(DataGiver.getEmail())
                .typeSubject(DataGiver.getShortText())
                .typeMessage(DataGiver.getLongText())
                .uploadFile(FILE_PATH)
                .clickSubmitButton()
                .clickOkJSWindow()
                .assertThatAlertLabelIsDisplayed(CONTACT_US_SUCCESS_MESSAGE);
    }

    @Test
    @Issue("DEFECT-44")
    @TmsLink("TC-7")
    @Severity(SeverityLevel.NORMAL)
    @Description("The goal of this test is try use contact form without email")
    public void asUserTryToUsingContactFormWithoutEmail() {
        DriverUtils.navigateTo(CONTACTUS_URL);

        ContactUsPage contactUsPage = new ContactUsPage();

        contactUsPage
                .typeName(DataGiver.getName())
                .typeSubject(DataGiver.getShortText())
                .typeMessage(DataGiver.getLongText())
                .clickSubmitButton()
                .assertThatAlertLabelIsNotDisplayed();
    }
}
