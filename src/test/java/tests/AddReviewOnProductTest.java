package tests;

import driver.manager.DriverUtils;
import io.qameta.allure.*;
import navigation.ApplicationURLs;
import org.testng.annotations.Test;
import pages.HomePage;
import utils.DataGiver;

public class AddReviewOnProductTest extends BaseTest{

    public AddReviewOnProductTest() {
        super();
    }

    @Test
    @Issue("DEFECT-61")
    @TmsLink("TC-19")
    @Severity(SeverityLevel.NORMAL)
    @Description("The goal of this test is try to add review on product")
    public void addReviewOnProduct() {
        DriverUtils.navigateTo(ApplicationURLs.APPLICATION_URL);

        HomePage homePage = new HomePage();

        homePage
                .clickOnViewProductLink()
                .typeName(DataGiver.getName())
                .typeEmail(DataGiver.getEmail())
                .typeReview(DataGiver.getLongText())
                .clickOnSubmitButton()
                .assertThatInformationIsDisplayed("Thank you for your review.");
    }

    @Test
    @Issue("DEFECT-62")
    @TmsLink("TC-20")
    @Severity(SeverityLevel.NORMAL)
    @Description("The goal of this test is try to add review on product without email")
    public void addReviewOnProductWithoutEmail() {
        DriverUtils.navigateTo(ApplicationURLs.APPLICATION_URL);

        HomePage homePage = new HomePage();

        homePage
                .clickOnViewProductLink()
                .typeName(DataGiver.getName())
                .typeReview(DataGiver.getLongText())
                .clickOnSubmitButton()
                .assertThatInformationIsNotDisplayed();
    }
}
