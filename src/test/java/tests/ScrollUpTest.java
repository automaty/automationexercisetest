package tests;

import driver.manager.DriverUtils;
import io.qameta.allure.*;
import navigation.ApplicationURLs;
import org.testng.annotations.Test;
import pages.HomePage;

public class ScrollUpTest extends BaseTest {

    public ScrollUpTest() {
        super();
    }

    @Test
    @Issue("DEFECT-21")
    @TmsLink("TC-21")
    @Severity(SeverityLevel.CRITICAL)
    @Description("The goal of this test is try scroll up using Arrow button")
    public void scrollUpUsingArrowButton() {
        DriverUtils.navigateTo(ApplicationURLs.APPLICATION_URL);

        HomePage homePage = new HomePage();

        homePage
                .scrollDown()
                .assertThatPageIsScrollingDown()
                .clickArrowButton()
                .assertThatPageIsScrollingUp();
    }

    @Test
    @Issue("DEFECT-28")
    @TmsLink("TC-22")
    @Severity(SeverityLevel.CRITICAL)
    @Description("The goal of this test is try scroll up without Arrow button")
    public void scrollUpWithoutArrowButton() {
        DriverUtils.navigateTo(ApplicationURLs.APPLICATION_URL);

        HomePage homePage = new HomePage();

        homePage
                .scrollDown()
                .assertThatPageIsScrollingDown()
                .scrollUp()
                .assertThatPageIsScrollingUp();
    }
}
