package tests;

import driver.manager.DriverUtils;
import io.qameta.allure.*;
import navigation.ApplicationURLs;
import org.testng.annotations.Test;
import pages.AllProductsPage;

public class ProductInformationTest extends BaseTest {

    public ProductInformationTest() {
        super();
    }

    @Test
    @Issue("DEFECT-5")
    @TmsLink("TC-8")
    @Severity(SeverityLevel.NORMAL)
    @Description("The goal of this test is select the product and check product information")
    public void asUserSelectProductAndCheckProductInformation() {
        DriverUtils.navigateTo(ApplicationURLs.ALL_PRODUCTS_URL);

        AllProductsPage allProductsPage = new AllProductsPage();

        allProductsPage
                .clickOnViewProductLink()
                .closeGoogleAds()
                .assertThatProductNameIs("Blue Top")
                .assertThatProductPriceIs("Rs. 500")
                .assertThatProductCategoryIs("Women > Tops")
                .assertThatProductAvailabilityIs("In Stock")
                .assertThatProductBrandIs("Polo")
                .assertThatProductConditionIs("New");
    }
}
