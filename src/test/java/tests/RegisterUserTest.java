package tests;

import driver.manager.DriverUtils;
import io.qameta.allure.*;
import org.testng.annotations.Test;
import pages.LoginPage;


import static navigation.ApplicationURLs.LOGINPAGE_URL;
import static utils.DataGiver.*;
import static utils.DataGiver.USER_NAME;

public class RegisterUserTest extends BaseTest {

    @Test
    @Issue("DEFECT-7")
    @TmsLink("TC-1")
    @Severity(SeverityLevel.CRITICAL)
    @Description("The goal of this test is try to register")
    public void asUserTryToRegister() {
        DriverUtils.navigateTo(LOGINPAGE_URL);

        LoginPage loginPage = new LoginPage();

        loginPage
                .typeName(getName())
                .typeEmail(getEmail())
                .clickSignupButton()
                .clickMrRadioButton()
                .typePassword(getPassword())
                .selectBirthDate("20", "2", "2000")
                .typeFirstName(getFirstName())
                .typeLastName(getLastName())
                .typeCompany(getCompany())
                .typeStreet(getStreet())
                .selectCountry("United States")
                .typeState(getState())
                .typeCity(getCity())
                .typeZipcode(getZipcode())
                .typeMobileNumber(getMobileNumber())
                .clickCreateAccountButton()
                .assertThatAccountCreatedLabelIsDisplayed("ACCOUNT CREATED!");
    }

    @Test
    @Issue("DEFECT-9")
    @TmsLink("TC-2")
    @Severity(SeverityLevel.CRITICAL)
    @Description("The goal of this test is try to register with exist email")
    public void asUserTryToRegisterWithExistingEmail() {
        DriverUtils.navigateTo(LOGINPAGE_URL);

        LoginPage loginPage = new LoginPage();

        loginPage
                .typeName(USER_NAME)
                .typeEmail(CORRECT_EMAIL)
                .clickSignupButton();
        loginPage
                .assertThatEmailAddressAlreadyExistMessageIsDisplayed("Email Address already exist!");
    }
}
