package tests;

import driver.manager.DriverUtils;
import io.qameta.allure.*;
import navigation.ApplicationURLs;
import org.aspectj.lang.annotation.DeclareMixin;
import org.testng.annotations.Test;
import pages.CartPage;
import pages.HomePage;
import utils.DataGiver;

import static navigation.ApplicationURLs.APPLICATION_URL;
import static utils.DataGiver.SUBSCRIPTION_SUCCESS_MESSAGE;

public class VerifySubscriptionTest extends BaseTest {

    public VerifySubscriptionTest() {
        super();
    }

    @Test
    @Issue("DEFECT-13")
    @TmsLink("TC-10")
    @Severity(SeverityLevel.NORMAL)
    @Description("The goal of this test is verify subscription on Home Page")
    public void asUserVerifySubscriptionInHomePage() {
        DriverUtils.navigateTo(APPLICATION_URL);

        HomePage homePage = new HomePage();

        homePage
                .typeSubscribeEmail(DataGiver.getEmail())
                .clickSubscribeButton()
                .assertThatSuccessSubscribeMessageIsDisplayed(SUBSCRIPTION_SUCCESS_MESSAGE);
    }

    @Test
    @Issue("DEFECT-29")
    @TmsLink("TC-11")
    @Severity(SeverityLevel.NORMAL)
    @Description("The goal of this test is verify subscription on Cart Page")
    public void asUserVerifySubscriptionInCartPage() {
        DriverUtils.navigateTo(ApplicationURLs.CARTPAGE_URL);

        CartPage cartPage = new CartPage();

        cartPage
                .typeSubscribeEmail(DataGiver.getEmail())
                .clickSubscribeButton()
                .assertThatSuccessSubscribeMessageIsDisplayed(SUBSCRIPTION_SUCCESS_MESSAGE);
    }
}
