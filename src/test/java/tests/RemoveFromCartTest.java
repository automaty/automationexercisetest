package tests;

import driver.manager.DriverUtils;
import io.qameta.allure.*;
import navigation.ApplicationURLs;
import org.testng.annotations.Test;
import pages.AllProductsPage;

public class RemoveFromCartTest extends BaseTest {

    public RemoveFromCartTest() {
        super();
    }

    @Test
    @Issue("DEFECT-64")
    @TmsLink("TC-16")
    @Severity(SeverityLevel.CRITICAL)
    @Description("The goal of this test is try to remove product from cart")
    public void asUserTryToRemoveProductFromCart() {
        int quantity = 2;
        DriverUtils.navigateTo(ApplicationURLs.ALL_PRODUCTS_URL);

        AllProductsPage allProductsPage = new AllProductsPage();

        allProductsPage
                .addProductToCart(quantity)
                .assertThatTheNumberOfProductInTheListIsEquals(quantity)
                .removeProduct()
                .assertThatTheNumberOfProductInTheListIsEquals(quantity - 1);
    }

    @Test
    @Issue("DEFECT-67")
    @TmsLink("TC-17")
    @Severity(SeverityLevel.CRITICAL)
    @Description("The goal of this test is try to remove one product of many")
    public void asUserTryToRemoveOneProductOfMany() {
        int quantity = 2;
        DriverUtils.navigateTo(ApplicationURLs.ALL_PRODUCTS_URL);

        AllProductsPage allProductsPage = new AllProductsPage();

        allProductsPage
                .addTheSameProductToCart(quantity)
                .assertThatTheQuantityOfProductIs(quantity)
                .removeProduct()
                .assertThatTheCartIsEmpty();
    }

    @Test
    @Issue("DEFECT-63")
    @TmsLink("TC-18")
    @Severity(SeverityLevel.CRITICAL)
    @Description("The goal of this test is try to remove all product from Cart")
    public void asUserTryToRemoveAllProductFromCart() {
        int quantity = 5;
        DriverUtils.navigateTo(ApplicationURLs.ALL_PRODUCTS_URL);

        AllProductsPage allProductsPage = new AllProductsPage();

        allProductsPage
                .addProductToCart(quantity)
                .assertThatTheNumberOfProductInTheListIsEquals(quantity)
                .removeAllProduct(quantity)
                .assertThatTheCartIsEmpty();
    }
}
