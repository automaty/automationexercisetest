package tests;

import driver.manager.DriverUtils;
import io.qameta.allure.*;
import org.testng.annotations.Test;
import pages.LoginPage;
import utils.DataGiver;

import static navigation.ApplicationURLs.LOGINPAGE_URL;
import static utils.DataGiver.INCORRECT_LOGIN;
import static utils.DataGiver.USER_NAME;

public class LoginUserTest extends BaseTest{

    public LoginUserTest() {
        super();
    }

    @Test
    @Issue("DEFECT-11")
    @TmsLink("TC-3")
    @Severity(SeverityLevel.BLOCKER)
    @Description("The goal of this test is try to login with correct email and password")
    public void asUserTryToLoginWithCorrectEmailAndPassword() {
        DriverUtils.navigateTo(LOGINPAGE_URL);

        LoginPage loginPage = new LoginPage();

        loginPage
                .typeLoginEmail(DataGiver.CORRECT_EMAIL)
                .typePassword(DataGiver.CORRECT_PASSWORD)
                .clickLoginButton()
                .assertThatLoggedInLabelIsDisplayed(USER_NAME);
    }

    @Test
    @Issue("DEFECT-14")
    @TmsLink("TC-4")
    @Severity(SeverityLevel.CRITICAL)
    @Description("The goal of this test is try to login with incorrect email and password")
    public void asUserTryToLoginWithIncorrectEmailAndPassword() {
        DriverUtils.navigateTo(LOGINPAGE_URL);

        LoginPage loginPage = new LoginPage();

        loginPage
                .typeLoginEmail(DataGiver.getEmail())
                .typePassword(DataGiver.getPassword())
                .clickLoginButton();
        loginPage
                .assertThatIncorrectLoginMessageIsDisplayed(INCORRECT_LOGIN);
    }

    @Test
    @Issue("DEFECT-8")
    @TmsLink("TC-5")
    @Severity(SeverityLevel.CRITICAL)
    @Description("The goal of this test is try to logout ")
    public void asLoggedUserTryToLogout() {
        DriverUtils.navigateTo(LOGINPAGE_URL);

        LoginPage loginPage = new LoginPage();

        loginPage
                .typeLoginEmail(DataGiver.CORRECT_EMAIL)
                .typePassword(DataGiver.CORRECT_PASSWORD)
                .clickLoginButton()
                .assertThatLoggedInLabelIsDisplayed(USER_NAME)
                .clickOnLogoutLink()
                .assertThatPageTitleIsLogin();
    }
}
