package tests;

import driver.manager.DriverUtils;
import io.qameta.allure.*;
import navigation.ApplicationURLs;
import org.testng.annotations.Test;
import pages.AllProductsPage;

import java.io.IOException;

public class AddToCartTest extends BaseTest {

    public AddToCartTest() {
        super();
    }

    @Test
    @Issue("DEFECT-4")
    @TmsLink("TC-12")
    @Severity(SeverityLevel.CRITICAL)
    @Description("The goal of this test is try to add 7 products to cart")
    public void asUserTryToAddProductToCart() {
        DriverUtils.navigateTo(ApplicationURLs.ALL_PRODUCTS_URL);

        AllProductsPage allProductsPage = new AllProductsPage();

        allProductsPage
                .addProductToCart(7)
                .assertThatTheNumberOfProductInTheListIsEquals(7);
    }

    @Test
    @Issue("DEFECT-8")
    @TmsLink("TC-13")
    @Severity(SeverityLevel.CRITICAL)
    @Description("The goal of this test is try to add products from excel file to cart")
    public void asUserTryToAddProductToCartFromExcel() throws IOException {
        DriverUtils.navigateTo(ApplicationURLs.ALL_PRODUCTS_URL);

        AllProductsPage allProductsPage = new AllProductsPage();

        allProductsPage
                .addProductsToCartFromExcel("testData.xlsx", "AddToCart")
                .assertThatTheProductsFromTheExcelAreInTheCart("testData.xlsx", "AddToCart");
    }
}
