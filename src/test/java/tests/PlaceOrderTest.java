package tests;

import driver.manager.DriverUtils;
import io.qameta.allure.*;
import navigation.ApplicationURLs;
import org.testng.annotations.Test;
import pages.AllProductsPage;
import pages.LoginPage;
import utils.DataGiver;

import static utils.DataGiver.USER_NAME;

public class PlaceOrderTest extends BaseTest {

    public PlaceOrderTest() {
        super();
    }

    //TODO refactor code

    @Test
    @Issue("DEFECT-21")
    @TmsLink("TC-14")
    @Severity(SeverityLevel.CRITICAL)
    @Description("The goal of this test is try to place order: login before checkout")
    public void asUserTryToPlaceOrder() {
        DriverUtils.navigateTo(ApplicationURLs.LOGINPAGE_URL);

        LoginPage loginPage = new LoginPage();

        loginPage
                .typeLoginEmail(DataGiver.CORRECT_EMAIL)
                .typePassword(DataGiver.CORRECT_PASSWORD)
                .clickLoginButton()
                .assertThatLoggedInLabelIsDisplayed(USER_NAME)
                .clickProductsLink()
                .closeGoogleAds()
                .addProductToCart(3)
                .assertThatTheNumberOfProductInTheListIsEquals(3)
                .clickCheckoutButton()
                .clickPlaceOrderButton()
                .typeCardName(DataGiver.getCardName())
                .typeCardNumber(DataGiver.getCardNumber())
                .typeCVC(DataGiver.getCVC())
                .typeExpirationMonth(DataGiver.getExpirationCardMonth())
                .typeExpirationYear(DataGiver.getExpirationCardYear())
                .clickSubmitButton()
                .assertThatOrderPlacedInformationIsDisplayed("ORDER PLACED!");
    }

    @Test
    @Issue("DEFECT-27")
    @TmsLink("TC-15")
    @Severity(SeverityLevel.CRITICAL)
    @Description("The goal of this test is try to place order: login while checkout")
    public void asUserTryToPlaceOrder_2() {
        DriverUtils.navigateTo(ApplicationURLs.ALL_PRODUCTS_URL);

        AllProductsPage allProductsPage = new AllProductsPage();

        allProductsPage
                .addProductToCart(4)
                .assertThatTheNumberOfProductInTheListIsEquals(4)
                .clickCheckoutButton()
                .clickRegisterLoginButton()
                .typeLoginEmail(DataGiver.CORRECT_EMAIL)
                .typePassword(DataGiver.CORRECT_PASSWORD)
                .clickLoginButton()
                .assertThatLoggedInLabelIsDisplayed(USER_NAME)
                .clickCartLink()
                .clickCheckoutButton()
                .clickPlaceOrderButton()
                .closeGoogleAds()
                .typeCardName(DataGiver.getCardName())
                .typeCardNumber(DataGiver.getCardNumber())
                .typeCVC(DataGiver.getCVC())
                .typeExpirationMonth(DataGiver.getExpirationCardMonth())
                .typeExpirationYear(DataGiver.getExpirationCardYear())
                .clickSubmitButton()
                .assertThatOrderPlacedInformationIsDisplayed("ORDER PLACED!");
    }
}
