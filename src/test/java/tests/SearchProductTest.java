package tests;

import driver.manager.DriverUtils;
import io.qameta.allure.*;
import navigation.ApplicationURLs;
import org.testng.annotations.Test;
import pages.AllProductsPage;

public class SearchProductTest extends BaseTest {

    public SearchProductTest() {
        super();
    }

    @Test
    @Issue("DFECT-17")
    @TmsLink("TC-9")
    @Severity(SeverityLevel.CRITICAL)
    @Description("The goal of this test is try to search product")
    public void asUserTryToSearchProduct() {
        DriverUtils.navigateTo(ApplicationURLs.ALL_PRODUCTS_URL);

        AllProductsPage allProductsPage = new AllProductsPage();
        String searchPhrase = "T-Shirt";

        allProductsPage
                .typeSearchProduct(searchPhrase)
                .clickOnSearchButton()
                .assertThatProductListHasSearchPhrase(searchPhrase);
    }
}
