package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.PageActions;

public class CheckoutPage extends BasePage {

    @FindBy(css = "a[href='/payment']")
    private WebElement placeOrderButton;

    @FindBy(css = ".modal-body [href='/login']")
    private WebElement registerLoginLink;

    public CheckoutPage() {
        super();
    }

    @Step("Click on Place Order button.")
    public PaymentPage clickPlaceOrderButton() {
        PageActions.scrollTo(placeOrderButton);
        click(placeOrderButton);
        log().info("Clicked on 'Place Order' button");
        return new PaymentPage();
    }

    public LoginPage clickRegisterLoginButton() {
        click(registerLoginLink);
        return new LoginPage();
    }
}
