package pages;

import io.qameta.allure.Step;
import org.apache.xmlbeans.impl.xb.xsdschema.All;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import utils.AdsCloser;
import utils.PageActions;
import utils.excel.Excel;
import waits.WaitForWebElement;

import java.io.IOException;
import java.util.List;

public class AllProductsPage extends BasePage{

    public AllProductsPage() {
        super();
    }

    @FindBy(css = "a[href='/product_details/1']")
    private WebElement viewProductLink;

    @FindBy(id = "search_product")
    private WebElement searchProductField;

    @FindBy(id = "submit_search")
    private WebElement submitSearchButton;

    @FindAll(@FindBy(css = ".single-products"))
    private List<WebElement> productList;

    @FindBy(css = ".close-modal")
    private WebElement continueShoppingButton;

    @FindBy(css = ".modal-content a[href='/view_cart']")
    private WebElement viewCartModalLink;

    @FindAll(@FindBy(css = ".overlay-content p"))
    private List<WebElement> productNameList;

    @Step("Click on link: View Product.")
    public ProductDetailsPage clickOnViewProductLink() {
        WaitForWebElement.waitUntilElementIsVisible(viewProductLink);
        PageActions.scrollTo(viewProductLink);
        click(viewProductLink);
        log().info("Clicked on 'View Product' link.");
        return new ProductDetailsPage();
    }

    @Step("Type into Search Product field: '{product}'.")
    public AllProductsPage typeSearchProduct(String product) {
        WaitForWebElement.waitUntilElementIsVisible(searchProductField);
        type(searchProductField, product);
        log().info("Typed into 'Search Product': {}", product);
        return this;
    }

    @Step("Click on Search Product Button.")
    public SearchResultPage clickOnSearchButton() {
        click(submitSearchButton);
        log().info("Clicked on Search Product Button.");
        return new SearchResultPage();
    }

    @Step("Add products to Cart from file: {fileName}.")
    public ViewCartPage addProductsToCartFromExcel(String fileName, String sheetName) throws IOException {
        List<String> excelProductName = new Excel(fileName, sheetName).getColumn(0);
        log().info("Add to Cart " + excelProductName.size() + " products.");
        for(int i=0; i<excelProductName.size(); i++) {
            for(int j=0; j<productNameList.size(); j++) {
                WebElement product = productNameList.get(j);
                if (excelProductName.get(i).equals(product.getAttribute("innerText"))) {
                    addToCart(productList.get(j));
                    continueShopping(excelProductName.size(), i);
                    break;
                }
            }
        }
        return new ViewCartPage();
    }

    @Step("Add {quantity} products to Cart.")
    public ViewCartPage addProductToCart(int quantity)  {
        log().info("Add to Cart " + quantity + " products.");
        if(productList.size() >= quantity) {
            for(int i = 0;  i < quantity; i++){
                addToCart(productList.get(i));
                log().info("Added to Cart product no.: " + i);
                continueShopping(quantity, i);
            }
        } else {
            throw new IllegalArgumentException("There are fewer items in the list!!!");
        }
        return new ViewCartPage();
    }

    private void continueShopping(int quantity, int counter) {
        WaitForWebElement.waitUntilElementIsClickable(continueShoppingButton);
        if(counter < quantity-1) click(continueShoppingButton);
        else click(viewCartModalLink);
    }

    private void addToCart(WebElement product) {
        PageActions.scrollTo(product);
        PageActions.moveTo(product);
        WebElement addToCartButton = find(product, By.cssSelector("div[class='product-overlay'] i"));
        WaitForWebElement.waitUntilElementIsClickable(addToCartButton);
        PageActions.JSClick(addToCartButton);
        log().info("Adding to Cart product: {}", product.getAttribute("innerText"));
    }

    @Step("Close Google Ads.")
    public AllProductsPage closeGoogleAds() {
        new AdsCloser().close();
        return this;
    }

    @Step("Add {quantity} the same product to Cart")
    public ViewCartPage addTheSameProductToCart(int quantity) {
        log().info("Add to Cart the same product  {} times", quantity);
        for(int i=0; i<quantity; i++) {
            addToCart(productList.get(0));
            log().info("Added to Cart product no.: " + i);
            continueShopping(quantity, i);
        }
        return new ViewCartPage();
    }
}

