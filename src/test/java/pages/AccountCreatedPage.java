package pages;

import assertions.AssertWebElement;
import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import waits.WaitForWebElement;

public class AccountCreatedPage extends BasePage {

    public AccountCreatedPage() {
        super();
    }

    @FindBy(css = "h2[data-qa='account-created'] b")
    private WebElement accountCreatedLabel;

    @Step("Check if info label '{infoLabel}' is displayed.")
    public AccountCreatedPage assertThatAccountCreatedLabelIsDisplayed(String infoLabel) {
        log().info("Checking if info label '{}' is displayed.", infoLabel);
        WaitForWebElement.waitUntilElementIsVisible(accountCreatedLabel);
        AssertWebElement.assertThat(accountCreatedLabel).isDisplayed().hasText(infoLabel);
        return this;
    }
}