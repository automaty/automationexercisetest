package pages;

import driver.manager.DriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

import static configuration.Configuration.configuration;

public abstract class BasePage {
    private Logger logger = LogManager.getLogger(this.getClass().getName());

    public BasePage() {
        PageFactory.initElements(DriverManager.getDriver(), this);
    }

    protected Logger log() {
        return logger;
    }

    public void type(final WebElement webElement, String string) {
        webElement.clear();
        webElement.sendKeys(string);
    }

    public void sendFile(final WebElement webElement, String filePath) {
        if(configuration.isRemoteRun()) ((RemoteWebElement)webElement).setFileDetector(new LocalFileDetector());
        type(webElement, filePath);
    }

    public void click(final WebElement webElement) {
        webElement.click();
    }

    public WebElement find(SearchContext searchContext, By locator) {
        return searchContext.findElement(locator);
    }
    public WebElement find(By locator) {
        return DriverManager.getDriver().findElement(locator);
    }

    public List<WebElement> finds(By locator) {
        return DriverManager.getDriver().findElements(locator);
    }

    public void select(WebElement webElement, String string) {
        new Select(webElement).selectByValue(string);
    }

}
