package pages;

import assertions.AssertWebElement;
import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.JSAlertWindows;
import utils.PageActions;
import waits.WaitForWebElement;

public class ContactUsPage extends BasePage {

    public ContactUsPage() {
        super();
    }

    @FindBy(name = "name")
    private WebElement nameField;

    @FindBy(name = "email")
    private WebElement emailField;

    @FindBy(name = "subject")
    private WebElement subjectField;

    @FindBy(id = "message")
    private WebElement messageTextArea;

    @FindBy(name = "upload_file")
    private WebElement uploadFileButton;

    @FindBy(name = "submit")
    private WebElement submitButton;

    @FindBy(css = ".status.alert-success")
    private WebElement alertSuccessLabel;

    @Step("Type into Name field: {name}")
    public ContactUsPage typeName(String name) {
        WaitForWebElement.waitUntilElementIsVisible(nameField);
        type(nameField, name);
        log().info("Typed into 'Name': {}", name);
        return this;
    }

    @Step("Type into Email field: {email}")
    public ContactUsPage typeEmail(String email) {
        type(emailField, email);
        log().info("Typed into 'Email': {}", email);
        return this;
    }

    @Step("Type into Subject field {subject}")
    public ContactUsPage typeSubject(String subject) {
        type(subjectField, subject);
        log().info("Typed into 'Subject': {}", subject);
        return this;
    }

    @Step("Type into Message area {message}")
    public ContactUsPage typeMessage(String message) {
        type(messageTextArea, message);
        log().info("Typed message into textarea: {}", message);
        return this;
    }

    @Step("Upload file from {filePath}")
    public ContactUsPage uploadFile(String filePath) {
        sendFile(uploadFileButton, filePath);
        log().info("Send file: {}", filePath);
        return this;
    }

    @Step("Click OK on JS Alert window")
    public ContactUsPage clickOkJSWindow() {
        new JSAlertWindows().confirm();
        log().info("Clicked on 'OK' button on JS Alert Window.");
        return this;
    }

    @Step("Click Submit button")
    public ContactUsPage clickSubmitButton() {
        PageActions.scrollTo(submitButton);
        click(submitButton);
        log().info("Clicked on 'Submit' button.");
        return this;
    }

    @Step("Check if alert message {alertText} is displayed")
    public ContactUsPage assertThatAlertLabelIsDisplayed(String alertText) {
        log().info("Checking if alert message: '{}' is displayed.", alertText);
        WaitForWebElement.waitUntilElementIsVisible(alertSuccessLabel);
        AssertWebElement.assertThat(alertSuccessLabel).isDisplayed().hasText(alertText);
        return this;
    }

    @Step("Check if success message is displayed")
    public ContactUsPage assertThatAlertLabelIsNotDisplayed() {
        log().info("Checking if alert message is not displayed.");
        //WaitForWebElement.waitUntilElementIsVisible(alertSuccessLabel);
        AssertWebElement.assertThat(alertSuccessLabel).isNotDisplayed();
        return this;
    }
}
