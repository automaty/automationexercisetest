package pages;

import assertions.AssertWebElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import utils.excel.Excel;
import waits.WaitForWebElement;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ViewCartPage extends BasePage {

    public ViewCartPage() {
        super();
    }

    @FindBy(css = "span#empty_cart")
    private WebElement cartIsEmptyMessage;

    @FindAll(@FindBy(css = ".cart_description a"))
    private List<WebElement> productNamesInTheCartList;

    @FindBy(css = ".cart_quantity_delete")
    private WebElement removeProductButton;

    @FindBy(css = ".check_out")
    private WebElement proceedToCheckoutButton;

    @Step("Click on: Remove button")
    public ViewCartPage removeProduct() {
        click(removeProductButton);
        log().info("Clicked on Remove button");
        return this;
    }

    @Step("Check if {quantity} products are on the list.")
    public ViewCartPage assertThatTheNumberOfProductInTheListIsEquals(int quantity) {
        log().info("Checking if  products are on the list: '{}'", quantity);
        int quant = 0;
        WaitForWebElement.wait(By.cssSelector("button.disabled"));
        List<WebElement> productsInTheCartList = finds(By.cssSelector("button.disabled"));
        System.out.println(productsInTheCartList.size());
        for(WebElement element : productsInTheCartList) {
            quant += Integer.parseInt(element.getText());
        }
        assertThat(quant).isEqualTo(quantity);
        return this;
    }

    @Step("Check if products from exel file are in the Cart. File name: {fileName}")
    public void assertThatTheProductsFromTheExcelAreInTheCart(String fileName, String sheetName) throws IOException {
        log().info("Checking if product from the excel file: '{}' are in the cart.", fileName);
        List<String> productName = new Excel(fileName, sheetName).getColumn(1);
        List<String> productNamesInTheCart = new ArrayList<>();
        for (WebElement element : productNamesInTheCartList) {
            productNamesInTheCart.add(element.getText());
        }
        assertThat(productName).hasSameElementsAs(productNamesInTheCart);
    }

    @Step("Click on Proceed to Checkout button")
    public CheckoutPage clickCheckoutButton() {
        click(proceedToCheckoutButton);
        log().info("Clicked on 'Proceed To Checkout' button");
        return new CheckoutPage();
    }
    @Step("Check if quantity of product in the Cart is {quantity}")
    public ViewCartPage assertThatTheQuantityOfProductIs(int quantity) {
        log().info("Checking if quantity of product on the Cart is {}", quantity);
        WebElement quantityButton = find(By.cssSelector("button.disabled"));
        AssertWebElement.assertThat(quantityButton).hasText(String.valueOf(quantity));
        return this;
    }

    @Step("Check if Cart is empty")
    public ViewCartPage assertThatTheCartIsEmpty() {
        log().info("Checking if Cart is empty");
        AssertWebElement.assertThat(cartIsEmptyMessage).isNotDisplayed();
        return this;
    }

    public ViewCartPage removeAllProduct(int quantity) {
        for(int i=0; i<quantity; i++) {
            removeProduct();
        }
        return this;
    }
}
