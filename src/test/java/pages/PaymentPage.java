package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.AdsCloser;
import waits.WaitForWebElement;

public class PaymentPage extends BasePage {

    @FindBy(css = "input[name='name_on_card']")
    private WebElement cardNameInput;

    @FindBy(css = "input[name='card_number']")
    private WebElement cardNumberInput;

    @FindBy(css = "input[name='cvc']")
    private WebElement cvcInput;

    @FindBy(css = "input[name='expiry_month']")
    private WebElement expiryMonthInput;

    @FindBy(css = "input[name='expiry_year']")
    private WebElement expiryYearInput;

    @FindBy(id = "submit")
    private WebElement submitButton;

    public PaymentPage() {
        super();
    }

    @Step("Type into Card Name field: {cardName}")
    public PaymentPage typeCardName(String cardName) {
        WaitForWebElement.waitUntilElementIsVisible(cardNameInput);
        type(cardNameInput, cardName);
        log().info("Type card name: {}", cardName);
        return this;
    }

    @Step("Type into Card Number field: {cardNumber}")
    public PaymentPage typeCardNumber(String cardNumber) {
        type(cardNumberInput, cardNumber);
        log().info("Type card number: {}", cardNumber);
        return this;
    }

    @Step("Type into CVC field: {cvc}")
    public PaymentPage typeCVC(String cvc) {
        type(cvcInput, cvc);
        log().info("Type cvc: {}", cvc);
        return this;
    }

    @Step("Type into Expiration Month field: { expirationMonth}")
    public PaymentPage typeExpirationMonth(String expirationMonth) {
        type(expiryMonthInput, expirationMonth);
        log().info("Type expiration month: {}", expirationMonth);
        return this;
    }

    @Step("Type into Expiration year: {expirationYear}")
    public PaymentPage typeExpirationYear(String expirationYear) {
        type(expiryYearInput, expirationYear);
        log().info("Type expiration year: {}", expirationYear);
        return this;
    }

    @Step("Click on Submit button")
    public PaymentDonePage clickSubmitButton() {
        click(submitButton);
        return new PaymentDonePage();
    }

    @Step("Close Google Ads.")
    public PaymentPage closeGoogleAds() {
        new AdsCloser().close();
        return this;
    }
}
