package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import utils.PageActions;
import waits.WaitForWebElement;

public class SignupPage extends BasePage {

    public SignupPage() {
        super();
    }

    @FindBy(id = "id_gender1")
    private WebElement mrRadioButton;

    @FindBy(id = "password")
    private WebElement passwordField;

    @FindBy(id = "days")
    private WebElement birthDayList;

    @FindBy(id = "months")
    private WebElement birthMonthList;

    @FindBy(id = "years")
    private WebElement birthYearList;

    @FindBy(id = "newsletter")
    private WebElement newsletterCheckbox;

    @FindBy(id = "first_name")
    private WebElement firstNameField;

    @FindBy(id = "last_name")
    private WebElement lastNameField;

    @FindBy(id = "company")
    private WebElement companyField;

    @FindBy(id = "address1")
    private WebElement address1Field;

    @FindBy(id = "address2")
    private WebElement address2Field;

    @FindBy(id = "country")
    private WebElement countryList;

    @FindBy(id = "state")
    private WebElement stateField;

    @FindBy(id = "city")
    private WebElement cityField;

    @FindBy(id = "zipcode")
    private WebElement zipcodeField;

    @FindBy(id = "mobile_number")
    private WebElement mobileNumberField;

    @FindBy(css = "button[data-qa='create-account']")
    private WebElement createAccountButton;

    @Step("Check radiobutton 'Mr.'")
    public SignupPage clickMrRadioButton() {
        click(mrRadioButton);
        log().info("Checked 'Mr.' radiobutton.");
        return this;
    }

    @Step("Type into Password field: {password}")
    public SignupPage typePassword(String password) {
        WaitForWebElement.waitUntilElementIsVisible(passwordField);
        type(passwordField, password);
        log().info("Typing into 'Password': {}", password);
        return this;
    }

    @Step("Select birth day: {day}-{month}-{year}")
    public SignupPage selectBirthDate(String day, String month, String year) {
        select(birthDayList, day);
        select(birthMonthList, month);
        select(birthYearList, year);
        log().info("Selecting birth date: {}-{}-{}", day, month, year);
        return this;
    }

    @Step("Type into First Name field: {firstName}")
    public SignupPage typeFirstName(String firstName) {
        type(firstNameField, firstName);
        log().info("Typing into 'First Name': {}", firstName);
        return this;
    }

    @Step("Type into Last Name field: {lastName}")
    public SignupPage typeLastName(String lastName) {
        type(lastNameField, lastName);
        log().info("Typing into 'Last Name': {}", lastName);
        return this;
    }

    @Step("Type into Company field: {company}")
    public SignupPage typeCompany(String company) {
        type(companyField, company);
        log().info("Typing into 'Company': {}", company);
        return this;
    }

    @Step("Type into Street field: {street}")
    public SignupPage typeStreet(String street) {
        type(address1Field, street);
        log().info("Typing into 'Street': {}", street);
        return this;
    }

    @Step("Select Country: {country}")
    public SignupPage selectCountry(String country) {
        select(countryList, country);
        log().info("Selecting 'Country': {}", country);
        return this;
    }

    @Step("Type into State field: {state}")
    public SignupPage typeState(String state) {
        type(stateField, state);
        log().info("Typing into 'State': {}", state);
        return this;
    }

    @Step("Type into City field: {city}")
    public SignupPage typeCity(String city) {
        type(cityField, city);
        log().info("Typing into 'City': {}", city);
        return this;
    }

    @Step("Type into Zipcode field: {zipcode}")
    public SignupPage typeZipcode(String zipcode) {
        type(zipcodeField, zipcode);
        log().info("Typing into 'Zipcode': {}", zipcode);
        return this;
    }

    @Step("Type in to Mobile Number field: {mobileNumber}")
    public SignupPage typeMobileNumber(String mobileNumber) {
        type(mobileNumberField, mobileNumber);
        log().info("Typing into 'Mobile Number': {}", mobileNumber);
        return this;
    }

    @Step("Click on Create Account button.")
    public AccountCreatedPage clickCreateAccountButton() {
        WaitForWebElement.waitUntilElementIsClickable(createAccountButton);
        PageActions.scrollTo(createAccountButton);
        click(createAccountButton);
        log().info("Clicked on 'Create Account' button.");
        return new AccountCreatedPage();
    }
}
