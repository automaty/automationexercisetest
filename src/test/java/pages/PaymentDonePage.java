package pages;

import assertions.AssertWebElement;
import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PaymentDonePage extends BasePage {

    @FindBy(css = "h2[data-qa='order-placed']")
    private WebElement orderPlacedLabel;

    public PaymentDonePage() {
        super();
    }

    @Step("Check if message '{message}' is displayed")
    public PaymentDonePage assertThatOrderPlacedInformationIsDisplayed(String message) {
        log().info("Checking if message '{}' is displayed.", message);
        AssertWebElement.assertThat(orderPlacedLabel).isDisplayed().hasText(message);
        return this;
    }
}
