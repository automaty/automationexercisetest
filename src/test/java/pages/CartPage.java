package pages;

import assertions.AssertWebElement;
import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.PageActions;
import waits.WaitForWebElement;

public class CartPage extends BasePage {

    public CartPage() {
        super();
    }

    @FindBy(id = "susbscribe_email")
    private WebElement subscribeEmailField;

    @FindBy(id = "subscribe")
    private WebElement subscribeButton;

    @FindBy(css = "#success-subscribe div")
    private WebElement successSubscribeMessage;

    @Step("Type into Subscribe Email: {email}")
    public CartPage typeSubscribeEmail(String email) {
        WaitForWebElement.waitUntilElementIsVisible(subscribeEmailField);
        PageActions.scrollTo(subscribeEmailField);
        type(subscribeEmailField,email);
        log().info("Typing '{}' into subscribe field.", email);
        return this;
    }

    @Step("Click on Subscribe button.")
    public CartPage clickSubscribeButton() {
        click(subscribeButton);
        log().info("Clicked on 'subscribe' button.");
        return this;
    }

    @Step("Check if message {message} is displayed.")
    public CartPage assertThatSuccessSubscribeMessageIsDisplayed(String message) {
        log().info("Checking if message: '{}' is displayed.", message);
        AssertWebElement.assertThat(successSubscribeMessage).isDisplayed().hasText(message);
        return this;
    }
}
