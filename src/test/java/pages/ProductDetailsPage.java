package pages;

import assertions.AssertWebElement;
import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.AdsCloser;
import waits.WaitForWebElement;

public class ProductDetailsPage extends BasePage {

    public ProductDetailsPage() {
        super();
    }

    @FindBy(css = ".product-information")
    private WebElement productInformationLabel;

    @FindBy(css = ".product-information h2")
    private WebElement productNameLabel;

    @FindBy(css = ".product-information span > span")
    private WebElement productPriceLabel;

    @FindBy(css = ".product-information p")
    private WebElement productCategoryLabel;

    @FindBy(css = ".product-information span + p")
    private WebElement productAvailabilityLabel;

    @FindBy(css = ".product-information span + p + p")
    private WebElement productConditionLabel;

    @FindBy(css = ".product-information span + p + p + p")
    private WebElement productBrandLabel;

    @FindBy(id = "name")
    private WebElement nameField;

    @FindBy(id = "email")
    private WebElement emailField;

    @FindBy(id = "review")
    private WebElement reviewField;

    @FindBy(id = "button-review")
    private WebElement submitButton;

    @FindBy(css = ".alert-success span")
    private WebElement successSubmitInformation;

    @Step("Type into Name field: {name}")
    public ProductDetailsPage typeName(String name) {
        type(nameField, name);
        log().info("Typed into Name field: {}", name);
        return this;
    }

    @Step("Type into Email field: {email}")
    public ProductDetailsPage typeEmail(String email) {
        type(emailField, email);
        log().info("Typed into Email field: {}", email);
        return this;
    }

    @Step("Type review into Review field:{review}")
    public ProductDetailsPage typeReview(String review) {
        type(reviewField, review);
        log().info("Typed into Review field: {}", review);
        return this;
    }

    @Step("Click on Submit button")
    public ProductDetailsPage clickOnSubmitButton() {
        click(submitButton);
        log().info("Clicked on Submit button");
        return this;
    }

    @Step("Check if  information {information} is displayed")
    public void assertThatInformationIsDisplayed(String information) {
        log().info("Checking if information: {} is displayed.", information);
        WaitForWebElement.waitUntilElementIsVisible(successSubmitInformation);
        AssertWebElement.assertThat(successSubmitInformation).isDisplayed().hasText(information);
    }

    @Step("Check if information about review isn't displayed")
    public void assertThatInformationIsNotDisplayed() {
        log().info("Checking if information about review isn't displayed.");
        AssertWebElement.assertThat(successSubmitInformation).isNotDisplayed();
    }


    @Step("Check if product name is: {productName}")
    public ProductDetailsPage assertThatProductNameIs(String productName) {
        log().info("Checking of product name is: '{}'", productName);
        WaitForWebElement.waitUntilElementIsVisible(productNameLabel);
        AssertWebElement.assertThat(productNameLabel).isDisplayed().hasText(productName);
        return this;
    }

    @Step("Check if product price is: {productPrice}")
    public ProductDetailsPage assertThatProductPriceIs(String productPrice) {
        log().info("Checking if product price is: '{}'", productPrice);
        AssertWebElement.assertThat(productPriceLabel).isDisplayed().hasText(productPrice);
        return this;
    }

    @Step("Check if product category is: {productCategory}")
    public ProductDetailsPage assertThatProductCategoryIs(String productCategory) {
        log().info("Checking if product category is: '{}'", productCategory);
        AssertWebElement.assertThat(productCategoryLabel).isDisplayed().hasSubText(productCategory);
        return this;
    }

    @Step("Check if product availability is: {productAvailability} ")
    public ProductDetailsPage assertThatProductAvailabilityIs(String productAvailability) {
        log().info("Checking if product availability is: '{}'", productAvailability);
        AssertWebElement.assertThat(productAvailabilityLabel).isDisplayed().hasSubText(productAvailability);
        return this;
    }

    @Step("Check if product condition is: {productCondition}")
    public ProductDetailsPage assertThatProductConditionIs(String productCondition) {
        log().info("Checking if product availability is: '{}'", productCondition);
        AssertWebElement.assertThat(productConditionLabel).isDisplayed().hasSubText(productCondition);
        return this;
    }

    @Step("Check if product brand is: {productBrand}")
    public ProductDetailsPage assertThatProductBrandIs(String productBrand) {
        log().info("Checking if product brand is: '{}'", productBrand);
        AssertWebElement.assertThat(productBrandLabel).isDisplayed().hasSubText(productBrand);
        return this;
    }

    @Step("Close Google Ads")
    public ProductDetailsPage closeGoogleAds() {
        new AdsCloser().close();
        return this;
    }
}
