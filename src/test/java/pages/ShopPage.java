package pages;

import assertions.AssertWebElement;
import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import waits.WaitForWebElement;

public class ShopPage extends BasePage {

    public ShopPage() {
        super();
    }

    @FindBy(css = "ul[class*='navbar-nav'] b")
    private WebElement loggedInAsLabel;

    @FindBy(css = "a[href='/logout']")
    private WebElement logoutLink;

    @FindBy(css = "a[href='/products']")
    private WebElement productsLink;

    @FindBy(css = "a[href='/view_cart']")
    private WebElement cartLink;

    public WebElement getLoggedInAsLabel() {
        return loggedInAsLabel;
    }

    @Step("Check if user name '{userName}' is displayed in label Logged in...")
    public ShopPage assertThatLoggedInLabelIsDisplayed(String userName) {
        log().info("Checking if user name '{}' is displayed in label: 'Logged in as ...' ", userName);
        WaitForWebElement.waitUntilElementIsVisible(loggedInAsLabel);
        AssertWebElement.assertThat(loggedInAsLabel).isDisplayed().hasText(userName);
        return this;
    }

    @Step("Click on Logout link")
    public LoginPage clickOnLogoutLink() {
        click(logoutLink);
        log().info("Clicked on Logout link.");
        return new LoginPage();
    }

    @Step("Click on Product link")
    public AllProductsPage clickProductsLink() {
        click(productsLink);
        log().info("Clicked on Products link");
        return new AllProductsPage();
    }

    @Step("Click on Cart link")
    public ViewCartPage clickCartLink() {
        click(cartLink);
        log().info("Clicked on Cart link");
        return new ViewCartPage();
    }
}
