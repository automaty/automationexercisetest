package pages;

import assertions.AssertWebElement;
import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SearchResultPage extends BasePage {

    public SearchResultPage() {
        super();
    }

    @FindAll(@FindBy(css = ".productinfo p"))
    private List<WebElement> searchingProductList;

    @Step("Check if list of product contains the search phrase: {phrase}")
    public SearchResultPage assertThatProductListHasSearchPhrase(String phrase) {
        log().info("Checking if list of product contains the search phrase.");
        for (WebElement element : searchingProductList) {
            AssertWebElement.assertThat(element).isDisplayed().hasSubText(phrase);
        }
        return this;
    }
}
