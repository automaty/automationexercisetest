package pages;

import assertions.AssertWebElement;
import io.qameta.allure.Step;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import utils.PageActions;
import waits.WaitForWebElement;


public class HomePage extends BasePage {

    @FindBy(css = "a[href*='/login']")
    private WebElement loginLink;

    @FindBy(id = "susbscribe_email")
    private WebElement subscribeEmailField;

    @FindBy(id = "subscribe")
    private WebElement subscribeButton;

    @FindBy(css = "#success-subscribe div")
    private WebElement successSubscribeMessage;

    @FindBy(css = "p.pull-left")
    private WebElement copyrightLabel;

    @FindBy(css = ".fa-angle-up")
    private WebElement upArrowButton;

    @FindBy(css = "[href='#slider-carousel'] .fa-angle-left")
    private WebElement leftArrowButton;

    @FindBy(css = "[href='/product_details/2']")
    private WebElement viewProductLink;

    public HomePage() {
        super();
    }

    @Step("Click on Login link.")
    public LoginPage clickOnLoginLink() {
        WaitForWebElement.waitUntilElementIsClickable(loginLink);
        click(loginLink);
        log().info("Clicked on 'Login 'link");
        return new LoginPage();
    }

    @Step("Type into Subscribe Email field: {email}")
    public HomePage typeSubscribeEmail(String email) {
        WaitForWebElement.waitUntilElementIsVisible(subscribeEmailField);
        PageActions.scrollTo(subscribeEmailField);
        type(subscribeEmailField, email);
        log().info("Typing '{}' into subscribe field.", email);
        return this;
    }

    @Step("Click Subscribe button.")
    public HomePage clickSubscribeButton() {
        click(subscribeButton);
        log().info("Clicked on 'subscribe' button.");
        return this;
    }

    @Step("Check if message {message} is displayed.")
    public HomePage assertThatSuccessSubscribeMessageIsDisplayed(String message) {
        log().info("Checking if message: '{}' is displayed.", message);
        AssertWebElement.assertThat(successSubscribeMessage).isDisplayed().hasText(message);
        return this;
    }

    @Step("Scroll page down.")
    public HomePage scrollDown() {
        PageActions.scrollTo(copyrightLabel);
        return this;
    }

    @Step("Scroll page up")
    public HomePage scrollUp() {
        PageActions.scrollTo(loginLink);
        return this;
    }

    @Step("Click Up Arrow Button.")
    public HomePage clickArrowButton() {
        click(upArrowButton);
        log().info("Clicked on 'Arrow' button.");
        return this;
    }

    @Step("Check if page is scrolling up.")
    public HomePage assertThatPageIsScrollingUp() {
        log().info("Check if page is scrolling up");
        boolean isScrolling;
        try {
            click(leftArrowButton);
            isScrolling = true;
        }
        catch (ElementClickInterceptedException e) {
            isScrolling = false;
        }
        Assert.assertTrue(isScrolling);
        return this;
    }

    @Step("Check if page is scrolling down.")
    public HomePage assertThatPageIsScrollingDown() {
        log().info("Check if page is scrolling down");
        boolean isScrolling;
        try {
            click(subscribeButton);
            isScrolling = true;
        }
        catch (ElementClickInterceptedException e) {
            isScrolling = false;
        }
        Assert.assertTrue(isScrolling);
        return this;
    }


    @Step("Click on link: View Product")
    public ProductDetailsPage clickOnViewProductLink() {
        WaitForWebElement.waitUntilElementIsClickable(viewProductLink);
        PageActions.scrollTo(viewProductLink);
        click(viewProductLink);
        log().info("Clicked on 'View Product' link");
        return new ProductDetailsPage();
    }
}
