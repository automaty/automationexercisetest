package pages;

import assertions.AssertWebElement;
import driver.manager.DriverUtils;
import io.qameta.allure.Step;
import org.assertj.core.api.Assertions;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import waits.WaitForWebElement;

public class LoginPage extends BasePage {

    public LoginPage() {
        super();
    }

    /////////////////LOGIN///////////////////

    @FindBy(css = "div[class='login-form'] input[name='email']")
    private WebElement loginEmailField;

    @FindBy(name = "password")
    private WebElement passwordField;

    @FindBy(css = "div[class='login-form'] button")
    private WebElement loginButton;

    @FindBy(css = "form[action='/login'] p")
    private WebElement incorrectLoginWarningMessage;

    ////////////////SIGNUP///////////////////

    @FindBy(name = "name")
    private WebElement nameField;

    @FindBy(css = "div[class='signup-form'] input[name='email']")
    private WebElement emailSignupField;

    @FindBy(css = "div[class='signup-form'] button")
    private WebElement signupButton;

    @FindBy(css = "form[action='/signup'] p")
    private WebElement emailAlreadyExistMessage;


    @Step("Type into Email field: {email}")
    public LoginPage typeLoginEmail(String email) {
        WaitForWebElement.waitUntilElementIsVisible(loginEmailField);
        type(loginEmailField, email);
        log().info("Typed into 'Email': {}", email);
        return this;
    }

    @Step("Type into Password field: {password}")
    public LoginPage typePassword(String password) {
        type(passwordField, password);
        log().info("Typed into 'Password': {}", password);
        return this;
    }

    @Step("Click on Login button")
    public ShopPage clickLoginButton() {
        click(loginButton);
        log().info("Clicked on 'Login' button.");
        return new ShopPage();
    }

    @Step("Type into Name field: {name}")
    public LoginPage typeName(String name) {
        type(nameField, name);
        log().info("Typed into 'Name': {}", name);
        return this;
    }

    @Step("Type into Email field: {email}")
    public LoginPage typeEmail(String email) {
        type(emailSignupField,email);
        log().info("Typed into 'Email': {}", email);
        return this;
    }

    @Step("Click on Signup button")
    public SignupPage clickSignupButton() {
        click(signupButton);
        log().info("Clicked on 'Signup' button.");
        return new SignupPage();
    }

    @Step("Check if warning mesage '{warningMessage}' is displayed.")
    public LoginPage assertThatIncorrectLoginMessageIsDisplayed(String warningMessage) {
        log().info("Checking if warning message: '{}' is displayed.", warningMessage);
        WaitForWebElement.waitUntilElementIsVisible(incorrectLoginWarningMessage);
        AssertWebElement.assertThat(incorrectLoginWarningMessage).isDisplayed().hasText(warningMessage);
        return this;
    }

    @Step("Check if page title is: Automation Exercise - Signup / Login")
    public LoginPage assertThatPageTitleIsLogin() {
        log().info("Checking if page title is: 'Automation Exercise - Signup / Login'");
        Assertions.assertThat(DriverUtils.getPageTitle()).isEqualTo("Automation Exercise - Signup / Login");
        return this;
    }

    @Step("Check if warning message '{warningMessage}' is displayed.")
    public LoginPage assertThatEmailAddressAlreadyExistMessageIsDisplayed(String warningMessage) {
        log().info("Checking if warning message: {} is displayed.", warningMessage);
        WaitForWebElement.waitUntilElementIsVisible(emailAlreadyExistMessage);
        AssertWebElement.assertThat(emailAlreadyExistMessage).isDisplayed().hasText(warningMessage);
        return this;
    }
}
