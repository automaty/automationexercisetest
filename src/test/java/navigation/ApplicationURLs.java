package navigation;

import configuration.Configuration;

import static configuration.Configuration.*;

public class ApplicationURLs {
    public static final String APPLICATION_URL = configuration.appUrl();
    public static final String LOGINPAGE_URL = APPLICATION_URL + "/login";
    public static final String CONTACTUS_URL = APPLICATION_URL + "/contact_us";
    public static final String ALL_PRODUCTS_URL = APPLICATION_URL + "/products";
    public static final String CARTPAGE_URL = APPLICATION_URL + "/view_cart";
}
